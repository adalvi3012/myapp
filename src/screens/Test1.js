import React, { Component } from  'react';

import { View, Text } from 'react-native';

export default class Test1 extends Component{
    componentDidMount = () => {
      console.log('test1')
    };
    
    render(){
        return(
            <View>
                <Text style={{ fontSize: 20 }}>Test1 Screen</Text>
            </View>
        );
    }
}