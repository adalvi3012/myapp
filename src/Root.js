import { Navigation } from 'react-native-navigation';

import Test1 from './screens/Test1';
import Test2 from './screens/Test2';
import Test3 from './screens/Test3';
import Drawer from './screens/Drawer';

export function registerScreens(){
    Navigation.registerComponent('example.Test1', () => Test1);
    Navigation.registerComponent('example.Test2', () => Test2);
    Navigation.registerComponent('example.Test3', () => Test3);

    Navigation.registerComponent('example.Drawer', () => Drawer)
}
